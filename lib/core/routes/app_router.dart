import 'package:assignment/feature/dashboard/presentation/pages/dashboard_page.dart';
import 'package:auto_route/annotations.dart';

import 'auth_guard.dart';

@AdaptiveAutoRouter(preferRelativeImports: true, routes: [
  AdaptiveRoute(page: DashboardPage, initial: true, guards: [AuthGuard]),
])
class $AppRouter {}
