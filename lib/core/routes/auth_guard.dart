import 'package:assignment/core/routes/app_router.gr.dart';
import 'package:auto_route/auto_route.dart';
import 'package:injectable/injectable.dart';

@injectable
class AuthGuard extends AutoRouteGuard {
  @override
  void onNavigation(NavigationResolver resolver, StackRouter router) async {
    router.pushAndPopUntil(const DashboardPageRoute(),
        predicate: (predicate) => false);
  }
}
