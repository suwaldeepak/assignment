// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// InjectableConfigGenerator
// **************************************************************************

import 'package:get_it/get_it.dart' as _i1;
import 'package:injectable/injectable.dart' as _i2;
import 'package:rest_client/rest_client.dart' as _i5;

import '../../config/app_config.dart' as _i3;
import '../../feature/dashboard/data/repository/events_repository.dart' as _i8;
import '../../feature/dashboard/domain/controller/i_events_controller.dart'
    as _i9;
import '../../feature/dashboard/domain/repository/i_events_repository.dart'
    as _i7;
import '../../feature/dashboard/presentation/bloc/event_bloc.dart' as _i10;
import '../network/http_config.dart' as _i6;
import '../routes/auth_guard.dart' as _i4;
import 'register_module.dart' as _i11; // ignore_for_file: unnecessary_lambdas

// ignore_for_file: lines_longer_than_80_chars
/// initializes the registration of provided dependencies inside of [GetIt]
_i1.GetIt $initGetIt(_i1.GetIt get,
    {String? environment, _i2.EnvironmentFilter? environmentFilter}) {
  final gh = _i2.GetItHelper(get, environment, environmentFilter);
  final registerModule = _$RegisterModule();
  gh.lazySingleton<_i3.AppConfig>(() => _i3.AppConfig());
  gh.factory<_i4.AuthGuard>(() => _i4.AuthGuard());
  gh.factory<_i5.IFileUploadRepository>(() =>
      registerModule.fileUploadRepository(
          get<_i5.IHttpConfig>(instanceName: 'uploadConfig')));
  gh.singleton<_i5.IHttpConfig>(_i6.HttpConfig(get<_i3.AppConfig>()),
      instanceName: 'httpConfig');
  gh.factory<_i5.IHttpHelper>(() => registerModule
      .httpHelper(get<_i5.IHttpConfig>(instanceName: 'httpConfig')));
  gh.factory<_i7.IEventsRepository>(
      () => _i8.EventsRepository(get<_i5.IHttpHelper>()));
  gh.factory<_i9.IEventsController>(
      () => _i9.EventsController(get<_i7.IEventsRepository>()));
  gh.factory<_i10.EventBloc>(
      () => _i10.EventBloc(get<_i9.IEventsController>()));
  return get;
}

class _$RegisterModule extends _i11.RegisterModule {}
