import 'package:injectable/injectable.dart';
import 'package:rest_client/rest_client.dart';

@module
abstract class RegisterModule {
  @Injectable(as: IHttpHelper)
  HttpHelper httpHelper(
    @Named('httpConfig') IHttpConfig config,
  ) =>
      HttpHelper(config: config);

  @Injectable(as: IFileUploadRepository)
  FileUploadRepository fileUploadRepository(
    @Named('uploadConfig') IHttpConfig config,
  ) =>
      FileUploadRepository(config: config);
}
