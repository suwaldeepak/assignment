import 'package:flutter/material.dart';

const Color colorBrown = Color(0xFFAA9767);
const Color colorPink = Color(0xFFE50695);
const Color colorPrimary = Color(0xFF009698);
const Color colorLightGrey = Color(0xFFDADBDB);
const LinearGradient linearGradient = LinearGradient(
    colors: [Color(0xFF005C5D), Color(0xFF009395)],
    begin: Alignment.topCenter,
    end: Alignment.bottomCenter);
