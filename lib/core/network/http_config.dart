import 'package:assignment/config/app_config.dart';
import 'package:injectable/injectable.dart';
import 'package:rest_client/rest_client.dart';

@Named('httpConfig')
@Singleton(as: IHttpConfig)
class HttpConfig implements IHttpConfig {
  final AppConfig _appConfig;

  HttpConfig(this._appConfig);

  @override
  String contentType = 'application/json';

  @override
  String token = '';

  @override
  String get baseUrl => _appConfig.baseUrl;
}
