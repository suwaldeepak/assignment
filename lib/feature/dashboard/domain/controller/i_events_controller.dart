import 'package:assignment/feature/dashboard/data/model/event_response_model.dart';
import 'package:assignment/feature/dashboard/domain/repository/i_events_repository.dart';
import 'package:injectable/injectable.dart';

abstract class IEventsController {
  Future<List<EventsResponseModel>> getEvents(int page, int limit);
}

@Injectable(as: IEventsController)
class EventsController implements IEventsController {
  final IEventsRepository repository;

  EventsController(this.repository);

  @override
  Future<List<EventsResponseModel>> getEvents(int page, int limit) {
    return repository.getEvents(page, limit);
  }
}
