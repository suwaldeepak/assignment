import 'package:assignment/feature/dashboard/data/model/event_response_model.dart';

abstract class IEventsRepository {
  Future<List<EventsResponseModel>> getEvents(int page, int limit);
}
