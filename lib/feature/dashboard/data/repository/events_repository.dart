import 'package:assignment/feature/dashboard/data/endpoint/events_endpoint.dart';
import 'package:assignment/feature/dashboard/data/model/event_response_model.dart';
import 'package:assignment/feature/dashboard/domain/repository/i_events_repository.dart';
import 'package:injectable/injectable.dart';
import 'package:rest_client/rest_client.dart';

@Injectable(as: IEventsRepository)
class EventsRepository implements IEventsRepository {
  final IHttpHelper iHttpHelper;

  EventsRepository(this.iHttpHelper);

  @override
  Future<List<EventsResponseModel>> getEvents(int page, int limit) async {
    List<EventsResponseModel> list = [];
    final response = await iHttpHelper.request(
        EventsEndpoint(page, limit), BaseRequestModel());
    for (Map<String, dynamic> i in response) {
      list.add(EventsResponseModel.fromJson(i));
    }
    return list;
  }
}
