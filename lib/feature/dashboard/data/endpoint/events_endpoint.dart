import 'package:rest_client/rest_client.dart';

class EventsEndpoint implements IRequestEndPoint {
  final int page;
  final int limit;

  EventsEndpoint(
    this.page,
    this.limit,
  );

  @override
  RequestMethod method = RequestMethod.GET;

  @override
  String get url => 'events/?_page=$page&_limit=$limit';
}
