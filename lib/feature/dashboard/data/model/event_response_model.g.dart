// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'event_response_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

EventsResponseModel _$EventsResponseModelFromJson(Map<String, dynamic> json) =>
    EventsResponseModel(
      json['id'] as int,
      json['date'] as String?,
      json['name'] as String?,
      json['image'] as String?,
      json['match'] as int?,
      json['price'] as String?,
      json['title'] as String?,
      json['address'] as String?,
      json['profileImage'] as String?,
    );

Map<String, dynamic> _$EventsResponseModelToJson(
        EventsResponseModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'date': instance.date,
      'name': instance.name,
      'image': instance.image,
      'match': instance.match,
      'price': instance.price,
      'title': instance.title,
      'address': instance.address,
      'profileImage': instance.profileImage,
    };
