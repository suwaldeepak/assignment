import 'package:json_annotation/json_annotation.dart';

part 'event_response_model.g.dart';

@JsonSerializable()
class EventsResponseModel {
  final int id;
  final String? date;

  final String? name;

  final String? image;

  final int? match;

  final String? price;

  final String? title;

  final String? address;

  final String? profileImage;

  EventsResponseModel(this.id, this.date, this.name, this.image, this.match,
      this.price, this.title, this.address, this.profileImage);

  factory EventsResponseModel.fromJson(Map<String, dynamic> json) =>
      _$EventsResponseModelFromJson(json);

  Map<String, dynamic> toJson() => _$EventsResponseModelToJson(this);
}
