import 'dart:async';

import 'package:assignment/feature/dashboard/data/model/event_response_model.dart';
import 'package:assignment/feature/dashboard/domain/controller/i_events_controller.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:injectable/injectable.dart';
import 'package:meta/meta.dart';

part 'event_event.dart';
part 'event_state.dart';

@injectable
class EventBloc extends Bloc<EventEvent, EventState> {
  IEventsController controller;
  int page = 1;

  EventBloc(this.controller) : super(EventInitial());

  @override
  Stream<EventState> mapEventToState(
    EventEvent event,
  ) async* {
    if (event is EventFetchEvent) {
      if (event.isLoading) {
        yield EventFetchingState();
      }
      dynamic response;
      if (page < 4) {
        response = await controller.getEvents(page, event.limit);
        if (response.isNotEmpty) {
          yield EventFetchingSuccessState(response);
          page++;
          return;
        }
        yield EventFetchingFailureState('Error occurred');
      }
    }
  }
}
