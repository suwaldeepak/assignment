part of 'event_bloc.dart';

@immutable
abstract class EventState {}

class EventInitial extends EventState {}

class EventFetchingState extends EventState {}

class EventFetchingFailureState extends EventState {
  final String message;

  EventFetchingFailureState(this.message);
}

class EventFetchingSuccessState extends EventState {
  final List<EventsResponseModel> response;

  EventFetchingSuccessState(this.response);
}
