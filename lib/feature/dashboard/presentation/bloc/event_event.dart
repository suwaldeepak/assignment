part of 'event_bloc.dart';

@immutable
abstract class EventEvent {}

class EventFetchEvent extends EventEvent {
  final int limit;
  final bool isLoading;

  EventFetchEvent({
    required this.limit,
    this.isLoading = true,
  });
}
