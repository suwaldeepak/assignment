import 'package:assignment/core/injection/injection.dart';
import 'package:assignment/feature/dashboard/data/model/event_response_model.dart';
import 'package:assignment/feature/dashboard/presentation/bloc/event_bloc.dart';
import 'package:assignment/feature/dashboard/presentation/widget/event_item_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class DashboardPage extends StatefulWidget {
  const DashboardPage({Key? key}) : super(key: key);

  @override
  _DashboardPageState createState() => _DashboardPageState();
}

class _DashboardPageState extends State<DashboardPage>
    with TickerProviderStateMixin {
  late TabController _tabController;
  late ScrollController _controller;
  List<EventsResponseModel> allEventList = [];
  EventBloc eventBloc;

  _DashboardPageState() : eventBloc = getIt<EventBloc>();

  @override
  void initState() {
    super.initState();
    _tabController = TabController(length: 2, vsync: this);
    _controller = ScrollController()..addListener(_scrollListener);
  }

  _scrollListener() {
    if (_controller.offset >= _controller.position.maxScrollExtent &&
        !_controller.position.outOfRange) {
      eventBloc.add(EventFetchEvent(limit: 10, isLoading: false));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 1,
        title: const Text('Southampton'),
        centerTitle: true,
        bottom: TabBar(
          controller: _tabController,
          tabs: const <Widget>[
            Tab(
              text: 'FOR YOU',
            ),
            Tab(
              text: 'TRENDING',
            ),
          ],
        ),
      ),
      body: BlocProvider<EventBloc>(
        create: (BuildContext context) =>
            eventBloc..add(EventFetchEvent(limit: 10)),
        child: BlocBuilder<EventBloc, EventState>(
          builder: (BuildContext context, state) {
            if (state is EventFetchingState) {
              return const Center(
                child: CupertinoActivityIndicator(),
              );
            } else if (state is EventFetchingFailureState) {
              return Center(
                child: Text(state.message),
              );
            } else if (state is EventFetchingSuccessState) {
              allEventList.addAll(state.response);
              return TabBarView(
                controller: _tabController,
                children: <Widget>[
                  ListView.builder(
                    controller: _controller,
                    shrinkWrap: true,
                    itemCount: allEventList.length + 1,
                    itemBuilder: (BuildContext context, int index) {
                      if (index == allEventList.length) {
                        return const Center(
                          child: SpinKitThreeBounce(
                            color: Colors.green,
                            size: 30,
                          ),
                        );
                      }
                      return EventItemWidget(
                        eventsResponseModel: allEventList[index],
                      );
                    },
                  ),
                  const Center(
                    child: Text("Trending"),
                  ),
                ],
              );
            } else {
              return const SizedBox();
            }
          },
        ),
      ),
    );
  }
}
