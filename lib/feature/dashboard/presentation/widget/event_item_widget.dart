import 'package:assignment/core/constants/dummy_image.dart';
import 'package:assignment/feature/dashboard/data/model/event_response_model.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';
import 'package:styled_widget/styled_widget.dart';

class EventItemWidget extends StatelessWidget {
  final EventsResponseModel eventsResponseModel;

  const EventItemWidget({
    Key? key,
    required this.eventsResponseModel,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final PageController controller = PageController();
    return Column(
      children: [
        ListTile(
          leading: ClipRRect(
            borderRadius: BorderRadius.circular(20),
            child: CachedNetworkImage(
              imageUrl: eventsResponseModel.profileImage!,
              height: 40,
              width: 40,
            ),
          ),
          title: Row(
            children: [
              Text(eventsResponseModel.name!),
              const Icon(
                Icons.admin_panel_settings,
                color: Colors.green,
                size: 16,
              )
            ],
          ),
          subtitle: Row(
            children: [
              const Icon(
                Icons.location_on_outlined,
                size: 16,
                color: Colors.black,
              ),
              Text(
                eventsResponseModel.address!,
                maxLines: 1,
              ).expanded(),
            ],
          ),
          trailing: IconButton(
            icon: const Icon(
              CupertinoIcons.ellipsis,
              color: Colors.black,
            ),
            onPressed: () {},
          ),
        ),
        SizedBox(
          height: 180,
          child: Stack(
            children: [
              PageView(
                controller: controller,
                children: [
                  CachedNetworkImage(
                    imageUrl: eventsResponseModel.image!,
                    fit: BoxFit.cover,
                  ),
                  CachedNetworkImage(
                    imageUrl: defaultImage,
                    fit: BoxFit.cover,
                  )
                ],
              ),
              Positioned(
                bottom: 16,
                left: MediaQuery.of(context).size.width / 2 - 5,
                child: SmoothPageIndicator(
                  controller: controller,
                  // PageController
                  count: 2,
                  effect: const JumpingDotEffect(
                    activeDotColor: Colors.white,
                    dotHeight: 6,
                    spacing: 4,
                    dotWidth: 6,
                  ), // your preferred effect
                ),
              ),
              Positioned(
                top: 16,
                right: 16,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: const [
                    Icon(
                      Icons.favorite_outline_rounded,
                      size: 20,
                      color: Colors.white,
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              eventsResponseModel.date!,
              maxLines: 1,
              style: const TextStyle(
                color: Colors.green,
              ),
            ),
            Text(
              eventsResponseModel.title!,
              maxLines: 2,
              style: const TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.w700,
                fontSize: 20,
              ),
            ),
            const SizedBox(
              height: 4,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  '\$${eventsResponseModel.price}',
                ),
                Text(
                  '${eventsResponseModel.match}% Match',
                  style: TextStyle(
                    color: Colors.grey.shade500,
                  ),
                ),
              ],
            )
          ],
        ).padding(left: 16, right: 16, top: 8),
        const SizedBox(height: 16),
        Container(height: 8, color: Colors.grey.shade300)
      ],
    );
  }
}
