import 'package:dio/dio.dart';

import '../http_config.dart';

class RequestInterceptor {
  final IHttpConfig config;

  RequestInterceptor(
    this.config,
  );

  InterceptorsWrapper getInterceptor() {
    return InterceptorsWrapper(
      onRequest: (RequestOptions options,
          RequestInterceptorHandler requestInterceptorHandler) async {
        return Future.value(onRequest(options, requestInterceptorHandler));
      },
    );
  }

  Object onRequest(RequestOptions options,
      RequestInterceptorHandler requestInterceptorHandler) async {
    final token = await config.token;
    options.contentType = config.contentType;
    options.baseUrl = config.baseUrl;
    token != null ? options.headers['accessToken'] = token : options.headers;
    print(token);
    requestInterceptorHandler.next(options);
    return options;
  }
}
