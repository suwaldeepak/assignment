import 'dart:io';

import 'package:dio/adapter.dart';
import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:rest_client/rest_client.dart';

import 'interceptors/pretty_logger.dart';
import 'interceptors/request_interceptor.dart';

class DioBuilder {
  late Dio _dio;
  final IHttpConfig config;

  DioBuilder({required this.config});

  Dio build() {
    debugPrint('dio initialized ${config.baseUrl}');
    _dio = Dio();

    (_dio.httpClientAdapter as DefaultHttpClientAdapter).onHttpClientCreate =
        (HttpClient client) {
      client.badCertificateCallback =
          (X509Certificate cert, String host, int port) => true;
      return client;
    };
    // ..options.connectTimeout = config?.connectTimeout ?? 10000
    // ..options.receiveTimeout = config?.receiveTimeout ?? 10000
    _dio.interceptors.add(PrettyLoggerInterceptor().prettyDioLogger);
    _dio.interceptors.add(RequestInterceptor(config).getInterceptor());
    _dio.options.baseUrl = config.baseUrl;
    _dio.options.headers = {'apikey': 'B5Kyu48YRMS8m0U'};
    return _dio;
  }
}
